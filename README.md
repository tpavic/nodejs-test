node-test

Just a simple NodeJS Hello World with a test.
Commands

    npm install

    npm start

    npm test

------PIPELINE------

We are using integrated GitLab CI/CD pipeline, our goal is to deploy our app on stage and prod AWS Elastic Beanstalk environments.
These environments are created via terraform, terraform scripts and modules are inside of https://gitlab.com/tpavic/terraform-aws-elasticbeanstalk.git repo.
AWS Elastic Beanstalk provides us simplistic code/image upload, afterwhich service itself handles the deployment, load balancing, auto-scaling, application health monitoring,etc ...

Pipeline contains following 5 stages:
build, package, test, deploy to stage, deploy to prod.

Build stage essentialy runs lightweight node-alpine container, checkouts our repo, installs neccessary node packages needed for testing and running our app, at the end we run predefined unit test, which checks the correct behavior of the application.

After successful build, package stage builds an image of our app from Dockerfile and pushes it into the gitlab registry using "docker in docker image" and predefined Gitlab env variables, APP_VERSION variable is mapped to predefined env variable $CI_COMMIT_SHORT_SHA (first eight characters of the commit revision) and used as tag.

Testing stage contains our built docker image under services keyword, which can run as container in parallel with the Docker image that the image keyword defines, in this case it's curl image.
After setting the alias for our application container, with the curl command we perform an additional runtime test, which checks response code.


Since deployments to stage and prod environments are similar, we can specify a hidden .deploy job which we will be used as a template.
It runs aws-cli image needed to deploy our image to previously created S3 and Elastic Beanstalk environments.
With rules keyword we can again use predefined variables and set that this job can be run only when current branch running the pipeline is the default branch (main)
Also our main branch is protected meaning only way for commit changes in this branch is to issue a merge request from feature branch.
Next, we have a script field which in short uses gettext package for switching env variables from our environments in to Dockerrun.aws.json file that describes how to deploy a set of Docker containers as an Elastic Beanstalk application.
Next script step uploads json file to S3 bucket used by Beanstalk environments, and we are then using aws-cli to upload newest version of app and update the application with that latest version.
Last command is again runtime test.
That's it.

Picture shows defined variables inside of our repo:

![CI/CD](.pictures/variables.png)

Before deploying to production there is manual keyword in that job, which requires manual intervention before it runs, that way we can stop newest app release on it's way to prod environment, create some additional tests, etc ...

Lastly, every merge to the main branch triggers Slack webhook which then sends notification to the test channel on our namespace.
![slack](.pictures/slack.png)
